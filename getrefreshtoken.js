const { exit } = require('process');
const axios = require('axios');
const url = require('url');
const redis = require('redis');
const readline = require('readline').createInterface({
    input: process.stdin,
    output: process.stdout
});

readline.question(`What's the app's client_id? `, client_id => {
    readline.question(`What's the app's client_secret? `, client_secret => {
        console.log(`Open URL: https://www.bungie.net/en/oauth/authorize?client_id=${client_id}&response_type=code`);
        readline.question(`What's the code in the URL of the web browser? `, code => {
            axios.post("https://www.bungie.net/platform/app/oauth/token/", new url.URLSearchParams({
                    grant_type: "authorization_code",
                    code: code,
                    client_id: client_id,
                    client_secret: client_secret
                }).toString()).then(resp => {
                let refreshtoken = {
                    token: resp.data.refresh_token,
                    expires: resp.data.refresh_expires_in + (new Date()).getTime() / 1000
                };
                console.log(JSON.stringify(refreshtoken))
                readline.question(`Would you like to save this in the local redis database? (y/n)`, yn => {
                    if (yn == "y" || yn == "Y") {
                        const client = redis.createClient({
                            url: 'redis://localhost:6379'
                        });
                        client.on('error', (err) => console.log('Redis Client Error', err));
                        client.connect().then(() => {
                            client.set("api-script:refreshtoken", JSON.stringify(refreshtoken)).then(() => {
                                exit();
                            });
                        })
                    } else {
                        exit();
                    }
                })
            })
        })
    })
})

