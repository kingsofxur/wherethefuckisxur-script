const redis = require('redis');

const client = redis.createClient({
    url: 'redis://redis:6379'
});
client.on('error', (err) => console.log('Redis Client Error', err));

//const axios = require('axios').default;
const request = require('request-promise-native');

const { calculateCycles } = require('./cycles.js');

var manifest;
var manifesturl;

var headers = {};

const xurLocs = [
    {
        'planet': 'Tower',
        'zone': 'The Hangar',
        'desc': 'Behind Dead Orbit',
        'img': '/images/tower_map_light.png'
    },
    {
        'planet': "Earth",
        'zone': 'Winding Cove', 
        'desc': 'On his cliff',
        'img': '/images/earth_map_light.png'
    },
    {
        'planet': 'Nessus',
        'zone': "Watcher's Grave",
        'desc': 'In his tree',
        'img': '/images/nessus_map_light.png'
    }
]

const morningReset = 17;
const xurDay = 5;
const weeklyReset = 2;

let refreshtoken;

client.connect().then(() => {
    runFetch();
})

function runFetch() {
    console.log('running main update function');
    updatemain().then(() => {
        setTimeout(runFetch, 60000);
    }, (error) => {
        console.log(error);
        setTimeout(runFetch, 60000);
    })
}

async function updatemain() {
    refreshtoken = JSON.parse(await client.get('api-script:refreshtoken')).token;

    console.log('updating patronizer...');
    await updatepatronizer();

    console.log('refreshing auth and manifest...');
    try {
        await authandmanifest();
    } catch (e) {
        console.log(e);
    }

    console.log('updating cycles...');
    await updatecycles();
    console.log('updating vendors...');
    await updatevendors();
    console.log('updating xur...');
    await updatexur();

    console.log('all done!\n');
}

async function authandmanifest() {
    console.log('refreshing auth...');
    let refreshurl = 'https://www.bungie.net/platform/app/oauth/token/';
    let refreshopts = {
        form: {
            'grant_type': 'refresh_token',
            'refresh_token': refreshtoken,
            'client_id': process.env.BUNGIE_API_ID,
            'client_secret': process.env.BUNGIE_API_SECRET
        },
        json: true
    };
    let refresh = await request.post(refreshurl, refreshopts);
    let newrefresh = {
        token: refresh.refresh_token,
        expires: ((new Date).getTime() / 1000) + 7776000
    }

    refreshtoken = refresh.refresh_token;
    await client.set('api-script:refreshtoken', JSON.stringify(newrefresh))
    headers = {
        'X-API-Key': process.env.BUNGIE_API_KEY,
        'Authorization': 'Bearer ' + refresh.access_token
    };

    console.log('checking manifest...');
    let manifestsurl = 'https://www.bungie.net/platform/Destiny2/Manifest';
    let manifestsopts = {
        headers: headers,
        json: true,
        gzip: true
    }
    let manifests = await request.get(manifestsurl, manifestsopts);

    let newmanifesturl = 'https://www.bungie.net' + manifests.Response.jsonWorldContentPaths.en;
    if (newmanifesturl != manifesturl) {
        console.log('new manifest version found, downloading...');
        manifesturl = newmanifesturl;
        let manifestopts = {
            headers: headers,
            json: true,
            gzip: true
        }
        manifest = await request.get(manifesturl, manifestopts);
    }
}

async function updatepatronizer() {
    if (await client.exists('wtfix:msg') == 0) {
        await client.json.set('wtfix:msg', '$', {
            patronizer: {
                action: "an entrepreneur with a big idea: prepackaged ice water. Think about it.",
                actions: [
                    "sneezing uncontrollably.",
                    "big chillin.",
                    "an entrepreneur with a big idea: prepackaged ice water. Think about it.",
                    "listening to ICP and drinking Faygo. Whoop whoop!",
                    "eating a Hot Pocket. Jealous?",
                    "LF2M. No noobs or squeakers. Must have Gjallarhorn."
                ],
                knockout: [5],
                lastUpdate: 1657170001210,
                name: "Tinlun",
                names: [
                    "NotDisliked",
                    "Dorkthrone",
                    "Soren",
                    "Oreckz",
                    "Rogue Calypso",
                    "Tinlun"
                ]
            },
            psa: "test psa",
            riff: "haha funny",
            xurmsg: "xur is here (or is he???)"
        })
    }
    let [patronizer] = await client.json.get('wtfix:msg', {
        path: '$.patronizer'
    });
    let lastUpdate = new Date(patronizer.lastUpdate);
    let currentTime = new Date()
    if (currentTime.getUTCHours() != lastUpdate.getUTCHours()) {
        if (!patronizer.knockout || patronizer.knockout.length == patronizer.names.length) {
            patronizer.knockout = [];
        }
        let isNew = false;
        let nameIndex;
        while (!isNew) {
            nameIndex = Math.floor(Math.random()*patronizer.names.length);
            if (!patronizer.knockout || !patronizer.knockout.includes(nameIndex)) {
                isNew = true;
            }
        }
        patronizer.lastUpdate = currentTime.getTime();
        patronizer.name = patronizer.names[nameIndex];
        patronizer.action = patronizer.actions[Math.floor(Math.random()*patronizer.actions.length)]
        patronizer.knockout.push(nameIndex);
        await client.json.set('wtfix:msg', '$.patronizer', patronizer);
        await client.publish('msg', '');
    }
}

async function updatecycles() {
    let cycles = calculateCycles();

    let nightfallparams = {
        'components': '204'
    }
    let nightfallurl = 'https://www.bungie.net/platform/Destiny2/' + process.env.BUNGIE_CHAR_PLAT + '/Profile/' + process.env.BUNGIE_CHAR_MEMBERID + '/Character/' + process.env.BUNGIE_CHAR_CHARID;
    let nightfallopts = {
        headers: headers,
        qs: nightfallparams,
        json: true,
    }
    try {
        let nightfallresp = await request.get(nightfallurl, nightfallopts);

        nightfallresp.Response.activities.data.availableActivities.forEach((activity) => {
            let activityDef = manifest.DestinyActivityDefinition[activity.activityHash];
            if (activityDef.originalDisplayProperties.name == "Nightfall") {
                cycles.activeordeal = activityDef.originalDisplayProperties.description;
                return;
            }
        })
    } catch (e) {
        console.log(e);
    }

    await client.json.set('wtfix:cycles', '$', cycles);
    await client.publish('cycles', '');
}

async function updatevendors() {
    let vendorparams = {
        'components': '400,401,402,300,301,302,303,304,305,306,307,308'
    }

    let vendorurl = 'https://www.bungie.net/platform/Destiny2/' + process.env.BUNGIE_CHAR_PLAT + '/Profile/' + process.env.BUNGIE_CHAR_MEMBERID + '/Character/' + process.env.BUNGIE_CHAR_CHARID + '/Vendors';
    let vendoropts = {
        headers: headers,
        qs: vendorparams,
        json: true,
        gzip: true
    }

    let vendorsavedata = {};

    try {
        let vendordata = await request.get(vendorurl, vendoropts);
        //console.log(vendordata.Response.vendors.data);
        if (!('Response' in vendordata)) {
            throw 'BungieDown';
        }
        let vendorsales = vendordata.Response.sales.data;
        let vendorcats = vendordata.Response.categories.data;
        let vendoritems = vendordata.Response.itemComponents;

        let vendorcatstoget = {
            '350061650': [1], //ADA-1
            '1735426333': [7], //Ana Bray
            '3982706173': [9], //Asher Mir
            '672118013': [8], //Banshee-44
            // '1265988377': [5], //Benedict 99-40
            '2398407866': [9], //Brother Vance
            '69482069': [3], //Commander Zavala
            '396892126': [8], //Devrim Kay
            '1616085565': [5], //Eris Morn
            // '919809084': [0, 1, 4], //Eva Levante
            '1576276905': [8], //Failsafe
            '3411552308': [0], //Lectern of Enchantment
            // '895295461': [6, 7], //Lord Saladin
            '3603221665': [6], //Lord Shaxx
            '1841717884': [3, 4], //Petra Venj
            '1062861569': [9], //Sloane
            '863940356': [3, 4, 5], //Spider
            '3347378076': [6, 7], //Suraya Hawthorne
            '3361454721': [21, 24], //Tess Everis
            '248695599': [2, 9], //The Drifter
            '2190858386': [0] //Xur
        }

        for (let key in vendorcatstoget) {
            if (key in vendoritems) {
                let allitemsockets = vendoritems[key].sockets.data;

                let cats = vendorcats[key].categories;
                let vendorcatsdata = []
                for (let cattogetkey in vendorcatstoget[key]) {
                    let cattoget = vendorcatstoget[key][cattogetkey];
                    let cat;
                    for (let vendorcatkey in cats) {
                        let vendorcat = cats[vendorcatkey];
                        if (vendorcat.displayCategoryIndex == cattoget) {
                            cat = vendorcat;
                        }
                    }
                    if (cat) {
                        let catdata = {
                            display: manifest.DestinyVendorDefinition[key].displayCategories[cat.displayCategoryIndex].displayProperties,
                            items: []
                        }
                        for (let itemkey in cat.itemIndexes) {
                            let itemindex = cat.itemIndexes[itemkey];
                            let saleitem = vendorsales[key].saleItems[itemindex];
                            let itemhash = saleitem.itemHash;

                            if (key == "2190858386") {
                                console.log(itemhash);
                            }

                            catdata.items.push(buildItemData(itemhash, saleitem, allitemsockets));
                        }
                        vendorcatsdata.push(catdata);
                    }
                }
                if (vendorcatsdata.length > 0) {
                    vendorsavedata[key] = {
                        display: manifest.DestinyVendorDefinition[key].displayProperties,
                        categories: vendorcatsdata
                    }
                }
            }
        }
    } catch (e) {
        console.log(e);
    }

    await client.json.set('wtfix:vendor', '$', vendorsavedata);
    await client.publish('vendor', '');
}

async function updatexur() {
    if (await client.exists('wtfix:xur') == 0) {
        await client.json.set('wtfix:xur', '$', {
            present: false
        })
    }
    let oldxur = await client.json.get('wtfix:xur', '$');
    if (isItXurTime()) {
        let xurparams = {
            'components': '400,401,402'
        }
    
        let xururl = 'https://www.bungie.net/platform/Destiny2/' + process.env.BUNGIE_CHAR_PLAT + '/Profile/' + process.env.BUNGIE_CHAR_MEMBERID + '/Character/' + process.env.BUNGIE_CHAR_CHARID + '/Vendors/2190858386';
        let xuropts = {
            headers: headers,
            qs: xurparams,
            json: true,
            gzip: true
        }
    
        try {
            let xurdata = await request.get(xururl, xuropts);
            //console.log(vendordata.Response.vendors.data);
            if (!('Response' in xurdata)) {
                throw 'BungieDown';
            }
            let xurlocindex = xurdata.Response.vendor.data.vendorLocationIndex;
            if (xurlocindex < 0 || xurlocindex > xurLocs.length - 1) {
                throw 'XurLocIndexOutOfRange';
            }

            if (oldxur.present == false || (oldxur.present == true && oldxur.found == false)) {
                let xurFile = {
                    ...xurLocs[xurlocindex],
                    present: true,
                    found: true,
                    lastUpdate: Date.now()
                }
                await client.json.set('wtfix:xur', '$', xurFile);
                await client.publish('xur', '');
            }
        } catch (e) {
            console.log(e);

            if (oldxur.present == false) {
                let xurFile = {
                    present: true,
                    found: false
                }
                await client.json.set('wtfix:xur', '$', xurFile);
                await client.publish('xur', '');
            }
        }
    } else {
        if (oldxur.present == true) {
            let xurFile = {
                present: false
            }
            await client.json.set('wtfix:xur', '$', xurFile);
            await client.publish('xur', '');
        }
    }
}

function buildItemData(itemhash, saleitem, itemsockets) {
    let itemdef = manifest.DestinyInventoryItemDefinition[itemhash];
    let itemdisplay = itemdef.displayProperties;
    let item = {
        display: itemdisplay,
        costs: [],
    }

    //Find price currency and quantity
    for (let costkey in saleitem.costs) {
        let cost = saleitem.costs[costkey];
        let costdef = manifest.DestinyInventoryItemDefinition[cost.itemHash];
        let costsave = {
            amount: cost.quantity,
            display: costdef.displayProperties
        }
        item.costs.push(costsave);
    }

    //Identify if item is a bounty or not
    if (itemdef.inventory.bucketTypeHash == '1345459588') {
        item.bounty = true;
    } else {
        item.bounty = false;
    }

    //If item has perks, get display properties of all of them
    if (itemdef.sockets) {
        for (let socketcatkey in itemdef.sockets.socketCategories) {
            let socketcat = itemdef.sockets.socketCategories[socketcatkey]
            let socketcatdef = manifest.DestinySocketCategoryDefinition[socketcat.socketCategoryHash];
            if (socketcatdef.categoryStyle == 1 || socketcatdef.categoryStyle == 6) {
                if (!item.sockets) {
                    item.sockets = [];
                }
                if (itemsockets[saleitem.vendorItemIndex]) {
                    let sockets = itemsockets[saleitem.vendorItemIndex].sockets;
                    for (let socketindexkey in socketcat.socketIndexes) {
                        let socketindex = socketcat.socketIndexes[socketindexkey];
                        let socket = sockets[socketindex];
                        if (socket.isVisible) {
                            let socketsave = {
                                plugs: []
                            }
                            if (socket.reusablePlugHashes) {
                                for (let plugkey in socket.reusablePlugHashes) {
                                    let plughash = socket.reusablePlugHashes[plugkey];
                                    let plugdef = manifest.DestinyInventoryItemDefinition[plughash];
                                    socketsave.plugs.push(plugdef.displayProperties);
                                }
                            } else if (socket.plugHash) {
                                let plughash = socket.plugHash;
                                let plugdef = manifest.DestinyInventoryItemDefinition[plughash];
                                socketsave.plugs.push(plugdef.displayProperties);
                            }
                            if (socketsave.plugs.length == 0) {
                                console.log('BIG OOF');
                            }
                            item.sockets.push(socketsave);
                        }
                    }
                }
            }
        }
    }

    //If item is class locked, identity the class it's locked to
    if (itemdef.classType != undefined) {
        if (itemdef.classType == 0) {
            item.class = 'Titan';
        } else if (itemdef.classType == 1) {
            item.class = 'Hunter';
        } else if (itemdef.classType == 2) {
            item.class = 'Warlock';
        }
    }

    return item;
}

function isItXurTime() {
    let time = new Date();
    let day = time.getUTCDay();
    let hour = time.getUTCHours();

    if ((weeklyReset < day && day < xurDay) || (day == weeklyReset && hour >= morningReset) || (day == xurDay && hour < morningReset)) {
        // 2 is Tuesday, 5 is Friday.
        return false;
    }
    else {
        return true;
    }
}